package com.mycompany.app;
import akka.actor.Props;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        Props props1 = Props.create(Actor.class);
        Props props2 = Props.create(Actor2.class);
        final ActorSystem system = ActorSystem.create("MySystem");
        final ActorRef myActor = system.actorOf(props1, "myActor");
        final ActorRef myActor2 = system.actorOf(props2, "myActor2");
        myActor.tell("Ping",myActor2);


    }
}



