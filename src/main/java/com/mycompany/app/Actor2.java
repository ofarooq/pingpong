package com.mycompany.app;

import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

/**
 * Created by BobTheBuilder on 12/08/15.
 */
public class Actor2 extends UntypedActor {
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    public void onReceive(Object message) throws Exception {
        if (message instanceof String) {

                log.info("Received String message: {}", message);
        } else
            unhandled(message);
    }
}